#
# This utility is for processing the New Zealand federal procurement records
# located at:
#
#  https://www.mbie.govt.nz/cross-government-functions/new-zealand-government-procurement-and-property/open-data/
#
# We must convert away from the original non UTF-8 encoding (cp1252).
#
# The schema for the original columns was given as:
#
# Header Column	        Description
# ==============================================================================
# Posting Agency	      Agency who have posted the tender.
#
# RFx ID	              Unique Identifier created by the GETS system	
#
# RFx Type              Type of tender. Limited to one of the following;
#                       Request for Proposal, Request for Quotations,
#                       Request for Tenders and Award Notice	
#
# Competition Type      Open or Closed Competition.	
#
# Title                 Descriptive title of the works, assigned by the tender
#                       poster.	Special characters may be present in this field.
#
# Reference Number	    A non-unique reference number, such as a project ID that
#                       can be used for multiple tenders. Can be a combination
#                       of alphabetical and numerical characters.	
#
# Open Date             Date tender opens.	
#
# Close Date            Date tender closes.	
#
# Awarded Date          Date tender awarded (including tenders marked
#                       'Not Awarded').	
#
# Department            Department within posting agencies that has submitted
#                       the tender.
#
# Tender Coverage       Who tender is for (All of Government, Cluster, On behalf
#                       of procurement agent, Sole Agency, Syndicated
#                       Opportunity)
#
# Prequalification      If 'Yes' is selected only tenders from prequalified
# Required?             suppliers will be accepted.	
#
# Alternative Physical  Address (physical or electronic) where tenders may also
# Tender Box Delivery   be submitted.	
# Address
#
# Overview
#
# Award Type            Tender will either be Awarded or Not Awarded.	If an
#                       agency wishes to award a tender to a manually received
#                       response they need to mark the Award Type field as not
#                       awarded so they can award it to a supplier off the
#                       system. However agencies may inadvertently mark this
#                       field as not award incorrectly.
#
# Comments              Free text field where the posting agency can give
#                       additional information bout the winning tender(s).
#
# Awarded Amount        Amount of the awarded.	
#
# Report Date           Date report was last updated.	
#
import csv
import json
import pandas as pd


ORIG_ENCODING = 'cp1252'

NOTICES_CSV_CURRENT = 'https://www.mbie.govt.nz/assets/Data-Files/NZGPP-GETS-Open-Data/GETS-award-notices.csv'
NOTICES_CSV_HISTORIC = 'https://www.mbie.govt.nz/assets/Data-Files/NZGPP-GETS-Open-Data/GETS-award-notices-historic.csv'

# New Zealand seems to be blocking wget requests to the above URLs now, so we
# can fall back to local downloads.
NOTICES_CSV_CURRENT_LOCAL = 'GETS-award-notices.csv'
NOTICES_CSV_HISTORIC_LOCAL = 'GETS-award-notices-historic.csv'


COLUMN_MAP = {
    'Posting Agency': 'posting_agency',
    'RFx ID': 'notice_id',
    'RFx Type': 'notice_type',
    'Competition Type': 'competition_type',
    'Title': 'title',
    'Reference Number': 'reference_number',
    'Open Date': 'open_date',
    'Close Date': 'close_date',
    'Awarded Date ': 'awarded_date',
    'Department': 'department',
    'Tender Coverage': 'tender_coverage',
    'Prequalification Required?': 'prequalification',
    'Alternative Physical Tender Box Delivery Address': 'alternate_address',
    'Overview': 'overview',
    'Award Type': 'award_type',
    'Comments': 'comments',
    'Awarded Amount': 'awarded_amount',
    'Report Date': 'report_date'
}

DTYPE_MAP = {
    'RFx ID': str
}

DATE_COLUMNS = ['open_date', 'close_date', 'awarded_date', 'report_date']


def export_df_to_csv(df, csv_filename):
    df = df.replace(r'\\r\\n', '; ', regex=True)
    df = df.replace(r'\\r', '; ', regex=True)
    df = df.replace(r'\\n', '; ', regex=True)

    df = df.replace(r'\r\n', '; ', regex=True)
    df = df.replace(r'\r', '; ', regex=True)
    df = df.replace(r'\n', '; ', regex=True)

    df = df.replace(r'\\', r'\\\\', regex=True)

    duplicated = df[['notice_id']].duplicated(keep='first')
    num_dropped = sum(duplicated)
    print('Dropping {} redundant rows.'.format(num_dropped))
    df = df.drop(df[duplicated].index)

    df.to_csv(csv_filename,
              index=False,
              escapechar='\\',
              quotechar='"',
              doublequote=False,
              sep='\t',
              quoting=csv.QUOTE_ALL)
    

def process_current_csv(
    csv_orig_filename='nz_award_notices_current.csv',
    csv_filename='nz_award_notice.csv',
    json_filename='nz_award_notices_current.json'):
    try:
        df = pd.read_csv(
            NOTICES_CSV_CURRENT, dtype=DTYPE_MAP, encoding=ORIG_ENCODING)
    except:
        print('Failed to download {}'.format(NOTICES_CSV_CURRENT))
        df = pd.read_csv(
            NOTICES_CSV_CURRENT_LOCAL, dtype=DTYPE_MAP,
            encoding=ORIG_ENCODING)
    df = df.rename(columns=COLUMN_MAP)
    df['notice_id'] = df['notice_id'].astype(str)
    for column in DATE_COLUMNS:
        df[column] = pd.to_datetime(
            df[column], format='%Y%m%d', errors='coerce')
    df.to_csv(csv_orig_filename, encoding='utf-8')
    df.to_json(json_filename, orient='records', indent=1, date_format='iso')
    export_df_to_csv(df, csv_filename)


def process_historic_csv(
    csv_orig_filename='nz_award_notices_historic.csv',
    csv_filename='nz_award_notice_historic.csv',
    json_filename='nz_award_notices_historic.json'):
    try:
        df = pd.read_csv(
            NOTICES_CSV_HISTORIC, dtype=DTYPE_MAP, encoding=ORIG_ENCODING)
    except:
        print('Failed to download {}'.format(NOTICES_CSV_HISTORIC))
        df = pd.read_csv(
            NOTICES_CSV_HISTORIC_LOCAL, dtype=DTYPE_MAP,
            encoding=ORIG_ENCODING)
    df = df.rename(columns=COLUMN_MAP)
    df['notice_id'] = df['notice_id'].astype(str)
    for column in DATE_COLUMNS:
        df[column] = pd.to_datetime(
            df[column], format='%Y%m%d', errors='coerce')
    df.to_csv(csv_orig_filename, encoding='utf-8')
    df.to_json(json_filename, orient='records', indent=1, date_format='iso')
    export_df_to_csv(df, csv_filename)


process_current_csv()
process_historic_csv()
